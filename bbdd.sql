CREATE DATABASE IF NOT EXISTS prueba_tecnica_idavinci;

USE prueba_tecnica_idavinci;

CREATE TABLE users(
	id int(10) auto_increment not null,
	role varchar(20),
	name varchar(100),
	surname varchar(200),
	email varchar (255),
	password varchar(255),
	created_at datetime,
	CONSTRAINT pk_users PRIMARY KEY(id)
)ENGINE=InnoDb;

CREATE TABLE superpersonas(
	id int(10) auto_increment not null,
	name varchar (255) unique not null,
	residence_city varchar(150),
	power smallint not null default 0,
	intelligence smallint not null default 0,
	health smallint not null default 0,
	kind varchar(25),
	active boolean,
	created_at datetime,
	updated_at datetime,
	created_by int(10),
	modified_by int(10),
	CONSTRAINT pk_superpersonas PRIMARY KEY(id)
)ENGINE=InnoDb;