<?php

namespace AppBundle\Tests\EventSubscriber;

use AppBundle\EventSubscriber\AuthorizationSubscriber;
use AppBundle\Services\JwtAuth;
use Mockery as m;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;

class AuthorizationSubscriberTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var AuthorizationSubscriber */
    private $sut;

    /** @var JwtAuth | m\MockInterface */
    private $jwtAuth;


    /** @test */
    public function getSubscribedEvents_andReturnEvents()
    {
        $events = ['kernel.request' => 'authorized'];

        $result = AuthorizationSubscriber::getSubscribedEvents();

        $this->assertEquals($events, $result);
    }


    /** @test */
    public function authorized_whenPathUserNew()
    {
        $event = m::mock(GetResponseEvent::class);
        $request = m::mock(Request::class);
        $path = '/user/new';

        $event->shouldReceive('getRequest')
            ->once()
            ->andReturn($request);

        $request->shouldReceive('getPathInfo')
            ->once()
            ->andReturn($path);

        $event->shouldReceive('getRequest')
            ->never();

        $this->sut->authorized($event);
    }

    /** @test */
    public function authorized_whenPathLogin()
    {
        $event = m::mock(GetResponseEvent::class);
        $request = m::mock(Request::class);
        $path = '/login';

        $event->shouldReceive('getRequest')
            ->once()
            ->andReturn($request);

        $request->shouldReceive('getPathInfo')
            ->once()
            ->andReturn($path);

        $event->shouldReceive('getRequest')
            ->never();

        $this->sut->authorized($event);
    }

    /** @test */
    public function authorized_whenPathHome()
    {
        $event = m::mock(GetResponseEvent::class);
        $request = m::mock(Request::class);
        $path = '/';

        $event->shouldReceive('getRequest')
            ->once()
            ->andReturn($request);

        $request->shouldReceive('getPathInfo')
            ->once()
            ->andReturn($path);

        $event->shouldReceive('getRequest')
            ->never();

        $this->sut->authorized($event);
    }

    /** @test */
    public function authorized_whenPathValid()
    {
        $event = m::mock(GetResponseEvent::class);
        $request = m::mock(Request::class);
        $path = '/superhero/new';
        $token = "eyJ0eXAiOiJKV1QiLCJhbGciOiJIUzI1NiJ9.eyJzdWIiOjE1LCJlbWFpbCI6Imc1NTRAaGhnLmNvbSIsIm5hbWUiOiJ
                  BbnRvbmlvIiwic3VybmFtZSI6ImRlbGdhZG8iLCJpYXQiOjE1NDQ3MDQ3MjQsImV4cCI6MTU0NTMwOTUyNH0.PFqT6NYzPKsfKYyPMc
                  jAjmCRPjBLrG2Lwe-ZjyUV9XQ";

        $event->shouldReceive('getRequest')
            ->once()
            ->andReturn($request);

        $request->shouldReceive('getPathInfo')
            ->once()
            ->andReturn($path);

        $request->shouldReceive('get')
            ->with('authorization', null)
            ->once()
            ->andReturn($token);

        $this->jwtAuth->shouldReceive('checkToken')
            ->with($token)
            ->once()
            ->andReturn(true);

        $this->sut->authorized($event);
    }

    /** @test
     *  @expectedException \Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException
     */
    public function authorized_whenPathValid_AuthorizationInvalid()
    {
        $event = m::mock(GetResponseEvent::class);
        $request = m::mock(Request::class);
        $path = '/superhero/new';
        $token = "token_erroneo";

        $event->shouldReceive('getRequest')
            ->once()
            ->andReturn($request);

        $request->shouldReceive('getPathInfo')
            ->once()
            ->andReturn($path);

        $request->shouldReceive('get')
            ->with('authorization', null)
            ->once()
            ->andReturn($token);

        $this->jwtAuth->shouldReceive('checkToken')
            ->with($token)
            ->once()
            ->andReturn(false);

        $this->sut->authorized($event);


    }


    protected function setUp()
    {
        $this->jwtAuth = m::mock(JwtAuth::class);

        $this->sut = new AuthorizationSubscriber($this->jwtAuth);
    }

    public function tearDown()
    {
        m::close();
    }

}