<?php

namespace AppBundle\Tests\Mapper;

use AppBundle\Mapper\SuperHeroMapper;
use AppBundle\Mapper\SuperHeroMapperInterface;
use BackendBundle\Entity\SuperHero;
use Mockery as m;

class SuperHeroMapperTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var SuperHeroMapperInterface */
    private $mapper;

    /** @test */
    public function mapSuperHero_whenSuperHero_andReturnMapperSuperHero()
    {
        $originSuperHero = new SuperHero();
        $originSuperHero->setName('super-hero-name');
        $originSuperHero->setResidenceCity('resident-city');
        $originSuperHero->setIntelligence(100);
        $originSuperHero->setHealth(60);
        $originSuperHero->setPower(20);

        $resultSuperHero = $this->mapper->mapSuperHero($originSuperHero, new SuperHero());

        $this->assertEquals($originSuperHero->getName(), $resultSuperHero->getName());
        $this->assertEquals($originSuperHero->getResidenceCity(), $resultSuperHero->getResidenceCity());
        $this->assertEquals($originSuperHero->getIntelligence(), $resultSuperHero->getIntelligence());
        $this->assertEquals($originSuperHero->getHealth(), $resultSuperHero->getHealth());
        $this->assertEquals($originSuperHero->getPower(), $resultSuperHero->getPower());
        $this->assertEquals(date('dmYHis'), $resultSuperHero->getUpdatedAt()->format('dmYHis'));
    }

    protected function setUp()
    {
        $this->mapper = new SuperHeroMapper();
    }


    public function tearDown()
    {
        m::close();
    }
}



