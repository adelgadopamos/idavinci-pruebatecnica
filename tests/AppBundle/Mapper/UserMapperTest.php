<?php

namespace AppBundle\Tests\Mapper;

use AppBundle\Mapper\UserMapper;
use AppBundle\Mapper\UserMapperInterface;
use BackendBundle\Entity\User;
use Mockery as m;

class UserMapperTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var UserMapperInterface */
    private $sut;

    /** @test */
    public function mapUser_whenUserWithPassword_andReturnMapperUser()
    {
        $originUser = new User();
        $originUser->setName('name-user');
        $originUser->setSurname('surname-user');
        $originUser->setEmail('email-user');
        $originUser->setPassword('12345');

        $resultUser = $this->sut->mapUser($originUser, new User());

        $this->assertEquals($originUser->getName(), $resultUser->getName());
        $this->assertEquals($originUser->getSurname(), $resultUser->getSurname());
        $this->assertEquals($originUser->getEmail(), $resultUser->getEmail());
        $this->assertEquals(hash('sha256', $originUser->getPassword()), $resultUser->getPassword());
    }

    /** @test */
    public function mapUser_whenUserWithoutPassword_andReturnMapperUser()
    {
        $originUser = new User();
        $originUser->setPassword(' ');
        $destinationUser = new User();
        $destinationUser->setPassword('987');

        $resultUser = $this->sut->mapUser($originUser, $destinationUser);

        $this->assertEquals('987', $resultUser->getPassword());
    }

    protected function setUp()
    {
        $this->sut = new UserMapper();
    }

    public function tearDown()
    {
        m::close();
    }

}