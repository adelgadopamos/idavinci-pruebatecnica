<?php

namespace AppBundle\Tests\Manager;

use AppBundle\Manager\UserManager;
use AppBundle\Manager\UserManagerInterface;
use AppBundle\Mapper\UserMapperInterface;
use BackendBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as m;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManagerTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var UserManagerInterface */
    private $sut;

    /** @var SerializerInterface | m\MockInterface */
    private $serializer;
    /** @var EntityManagerInterface | m\MockInterface */
    private $em;
    /** @var ValidatorInterface | m\MockInterface */
    private $validator;
    /** @var UserMapperInterface | m\MockInterface */
    private $userMapper;


    /** @test */
    public function newUser_whenUserValid_andReturnUser()
    {
        $json = '{
            "name" : "Antonio",
            "surname" : "Delgado",
            "email" : "example@domain.com",
            "password" : "12345",
        }';

        $request = m::mock(Request::class);
        $request
            ->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $user = new User();
        $user->setName('Antonio');
        $user->setSurname('Delgado');
        $user->setEmail('example@domain.com');
        $user->setPassword('12345');

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, User::class, 'json')
            ->once()
            ->andReturn($user);

        $user->setCreatedAt(new \DateTime("now"));
        $user->setRole('user');

        $this->validator
            ->shouldReceive('validate')
            ->with($user)
            ->once()
            ->andReturn([]);

        $result = $this->sut->newUser($request);

        $this->assertEquals($user, $result);
    }

    /** @test */
    public function newUser_whenUserNotValid_andReturnNull()
    {
        $json = '{
            "name" : "Antonio",
            "surname" : "Delgado",
            "email" : "example@domain.com",
            "password" : "12345",
        }';

        $request = m::mock(Request::class);
        $request
            ->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $user = new User();
        $user->setName('Antonio');
        $user->setSurname('Delgado');
        $user->setEmail('example@domain.com');
        $user->setPassword('12345');

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, User::class, 'json')
            ->once()
            ->andReturn($user);

        $user->setCreatedAt(new \DateTime("now"));
        $user->setRole('user');

        $this->validator
            ->shouldReceive('validate')
            ->with($user)
            ->once()
            ->andReturn([1, 1, 1]);

        $result = $this->sut->newUser($request);

        $this->assertNull($result);
    }

    /** @test */
    public function getUserToUpdate_whenUser_andReturnUserMapped()
    {
        $json = '{
            "name" : "Antonio",
            "surname" : "Delgado",
            "email" : "example@domain.com",
            "password" : "12345",
        }';

        $request = m::mock(Request::class);
        $request
            ->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $userUpdate = new User();
        $userUpdate->setName('Antonio');
        $userUpdate->setSurname('Delgado');
        $userUpdate->setEmail('example@domain.com');
        $userUpdate->setPassword('12345');

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, User::class, 'json')
            ->once()
            ->andReturn($userUpdate);

        $user = new User();
        $user->setName('Francisco');
        $user->setSurname('Delgado');
        $user->setEmail('example1@domain.com');
        $user->setPassword('12345');

        $userMapped = new User();
        $userMapped->setName('Antonio');
        $userMapped->setSurname('Delgado');
        $userMapped->setEmail('example@domain.com');
        $userMapped->setPassword('12345');

        $this->userMapper
            ->shouldReceive('mapUser')
            ->with($userUpdate, $user)
            ->once()
            ->andReturn($userMapped);

        $result = $this->sut->getUserToUpdate($request, $user);

        $this->assertEquals($userMapped, $result);

    }


    /** @test */
    public function checkExistsUser_whenUser_andReturnTrue()
    {
        $user = new User();
        $user->setName('Antonio');
        $user->setSurname('Delgado');
        $user->setEmail('example@domain.com');
        $user->setPassword('12345');

        $repository = m::mock(ObjectRepository::class);

        $this->em
            ->shouldReceive('getRepository')
            ->with(User::class)
            ->once()
            ->andReturn($repository);

        $repository->
        shouldReceive('findBy')
            ->with(['email' => $user->getEmail()])
            ->once()
            ->andReturn([]);

        $result = $this->sut->checkExistsUser($user);

        $this->assertEquals(true, $result);
    }

    /** @test */
    public function checkExistsUser_whenUserNotSet_andReturnFalse()
    {
        $user = new User();
        $user->setName('Antonio');
        $user->setSurname('Delgado');
        $user->setEmail('example@domain.com');
        $user->setPassword('12345');

        $repository = m::mock(ObjectRepository::class);

        $this->em
            ->shouldReceive('getRepository')
            ->with(User::class)
            ->once()
            ->andReturn($repository);

        $repository->
        shouldReceive('findBy')
            ->with(['email' => $user->getEmail()])
            ->once()
            ->andReturn([1, 1, 1]);

        $result = $this->sut->checkExistsUser($user);

        $this->assertNull($result);
    }


    /** @test */
    public function insertUser_whenUser_andReturnTrue()
    {
        $user = new User();
        $user->setRole('user');
        $user->setName('Antonio');
        $user->setSurname('Delgado');
        $user->setEmail('example@domain.com');
        $user->setPassword('12345');
        $user->setCreatedAt(new \DateTime('now'));

        $this->em
            ->shouldReceive('persist')
            ->with($user)
            ->once();

        $this->em
            ->shouldReceive('flush')
            ->once();

        $result = $this->sut->insertUser($user);

        $this->assertTrue($result);
    }

    /** @test */
    public function insertUser_whenUserNotSet_andReturnFalse()
    {
        $user = null;

        $result = $this->sut->insertUser($user);

        $this->assertFalse($result);
    }


    protected function setUp()
    {
        $this->serializer = m::mock(SerializerInterface::class);
        $this->em = m::mock(EntityManagerInterface::class);
        $this->validator = m::mock(ValidatorInterface::class);
        $this->userMapper = m::mock(UserMapperInterface::class);

        $this->sut = new UserManager(
            $this->serializer,
            $this->em,
            $this->validator,
            $this->userMapper
        );
    }

    public function tearDown()
    {
        m::close();
    }

}