<?php

namespace AppBundle\Tests\Manager;

use AppBundle\Manager\ResponseManager;
use AppBundle\Manager\ResponseManagerInterface;
use AppBundle\Services\HelpersInterface;
use Mockery as m;
use Symfony\Component\HttpFoundation\Response;

class ResponseManagerTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var ResponseManagerInterface */
    private $sut;

    /** @var HelpersInterface | m\MockInterface */
    private $helpers;

    /** @test */
    public function getArrayErrorResponse_whenMsg_andReturnHelpers()
    {
        $msg = 'Se ha producido un error';
        $data = [
            'status' => 'error',
            'code' => 400,
            'msg' => $msg
        ];

        $response = m::mock(Response::class);
        $this->helpers
            ->shouldReceive('json')
            ->with($data)
            ->once()
            ->andReturn($response);

        $resultResponse = $this->sut->getArrayErrorResponse($msg);

        $this->assertSame($response, $resultResponse);
    }

    /** @test */
    public function getArraySuccessResponse_whenMsg_andReturnHelpers()
    {
        $msg = 'Respuesta correcta';

        $data = [
            'status' => 'success',
            'code' => 200,
            'msg' => $msg
        ];

        $response = m::mock(Response::class);
        $this->helpers
            ->shouldReceive('json')
            ->with($data)
            ->once()
            ->andReturn($response);

        $resultResponse = $this->sut->getArraySuccessResponse($msg);

        $this->assertEquals($response, $resultResponse);
    }


    /** @test */
    public function getArraySuccessResponseObject_whenObject_andReturnHelpers()
    {
        $object = '{"nombre" : "Antonio", "apellido" : "Delgado", "edad": "26"}';

        $data = [
            'status' => 'success',
            'code' => 200,
            'object' => $object
        ];

        $response = m::mock(Response::class);
        $this->helpers
            ->shouldReceive('json')
            ->with($data)
            ->once()
            ->andReturn($response);

        $resultResponse = $this->sut->getArraySuccessResponseObject($object);

        $this->assertEquals($response, $resultResponse);
    }


    /** @test */
    public function getArraySuccessResponsePagination_whenPaginationParameters_andReturnHelpers()
    {
        $total_items_count = 21;
        $page = 1;
        $items_per_page = 10;
        $pagination = "Esto es una prueba";

        $data = [
            'status' => 'success',
            'code' => 200,
            'total_items_count' => $total_items_count,
            'page_actual' => $page,
            'items_per_page' => $items_per_page,
            'total_pages' => 3,
            'data' => $pagination
        ];

        $response = m::mock(Response::class);
        $this->helpers
            ->shouldReceive('json')
            ->with($data)
            ->once()
            ->andReturn($response);

        $resultResponse = $this->sut->getArraySuccessResponsePagination(
            $total_items_count,
            $page,
            $items_per_page,
            $pagination
        );

        $this->assertEquals($response, $resultResponse);
    }

    protected function setUp()
    {
        $this->helpers = m::mock(HelpersInterface::class);

        $this->sut = new ResponseManager($this->helpers);
    }

    public function tearDown()
    {
        m::close();
    }

}