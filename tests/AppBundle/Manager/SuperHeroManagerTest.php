<?php

namespace AppBundle\Tests\Manager;

use AppBundle\Manager\SuperHeroManager;
use AppBundle\Manager\SuperHeroManagerInterface;
use AppBundle\Mapper\SuperHeroMapperInterface;
use BackendBundle\Entity\SuperHero;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as m;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SuperHeroManagerTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var SuperHeroManagerInterface */
    private $sut;

    /** @var SerializerInterface | m\MockInterface */
    private $serializer;
    /** @var EntityManagerInterface | m\MockInterface */
    private $em;
    /** @var ValidatorInterface | m\MockInterface */
    private $validator;
    /** @var SuperHeroMapperInterface | m\MockInterface */
    private $superHeroMapper;


    /** @test */
    public function newSuperHero_whenValidResult_andReturnSuperhero()
    {
        $json = '{
            "name" : "bart",
            "residence_city" : "springfield",
            "health" : "80",
            "intelligence" : "10",
            "power" : "50",
            "kind" : "villano"
        }';

        $request = m::mock(Request::class);
        $request->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $superHero = new SuperHero();
        $superHero->setName("bart");
        $superHero->setResidenceCity('springfield');
        $superHero->setHealth(80);
        $superHero->setIntelligence(10);
        $superHero->setPower(50);
        $superHero->setKind('villano');

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, SuperHero::class, 'json')
            ->once()
            ->andReturn($superHero);

        $superHero->setCreatedAt(new \DateTime("now"));
        $superHero->setUpdatedAt(new \DateTime("now"));
        $superHero->setActive(true);
        $superHero->setCreatedBy(1);
        $superHero->setModifiedBy(1);

        $this->validator
            ->shouldReceive('validate')
            ->with($superHero)
            ->once()
            ->andReturn([]);

        $result = $this->sut->newSuperHero($request);

        $this->assertEquals($superHero, $result);
    }

    /** @test */
    public function newSuperHero_whenInvalidResult_andReturnNull()
    {
        $json = '{
            "name" : "bart",
            "residence_city" : "springfield",
            "health" : "80",
            "intelligence" : "10",
            "power" : "50",
            "kind" : "villano"
        }';

        $request = m::mock(Request::class);
        $request->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $superHero = new SuperHero();
        $superHero->setName("bart");
        $superHero->setResidenceCity('springfield');
        $superHero->setHealth(80);
        $superHero->setIntelligence(10);
        $superHero->setPower(50);
        $superHero->setKind('villano');

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, SuperHero::class, 'json')
            ->once()
            ->andReturn($superHero);

        $superHero->setCreatedAt(new \DateTime("now"));
        $superHero->setUpdatedAt(new \DateTime("now"));
        $superHero->setActive(true);
        $superHero->setCreatedBy(1);
        $superHero->setModifiedBy(1);

        $this->validator
            ->shouldReceive('validate')
            ->with($superHero)
            ->once()
            ->andReturn([1, 1, 1]);

        $superHeroRequest = $this->sut->newSuperHero($request);

        $this->assertNull($superHeroRequest);
    }


    /** @test */
    public function getSuperHeroToUpdate_whenSuperhero_returnMapSuperHero()
    {
        $json = '{
            "name" : "bart",
            "residence_city" : "springfield",
            "health" : "80",
            "intelligence" : "10",
            "power" : "50",
            "kind" : "villano"
        }';

        $request = m::mock(Request::class);
        $request->shouldReceive('get')
            ->with('json', null)
            ->once()
            ->andReturn($json);

        $superHeroUpdate = new SuperHero();
        $superHeroUpdate->setName("bart");
        $superHeroUpdate->setResidenceCity('springfield');
        $superHeroUpdate->setHealth(80);
        $superHeroUpdate->setIntelligence(10);
        $superHeroUpdate->setPower(50);
        $superHeroUpdate->setKind('villano');
        $superHeroUpdate->setCreatedAt(new \DateTime("now"));
        $superHeroUpdate->setUpdatedAt(new \DateTime("now"));
        $superHeroUpdate->setActive(true);
        $superHeroUpdate->setCreatedBy(1);
        $superHeroUpdate->setModifiedBy(1);

        $superHero = new SuperHero();
        $superHero->setName("bart");
        $superHero->setResidenceCity("Paris");
        $superHero->setHealth(65);
        $superHero->setIntelligence(55);
        $superHero->setPower(55);


        $superHeroMapped = new SuperHero();
        $superHeroMapped->setName('bart');
        $superHeroMapped->setResidenceCity('springfield');
        $superHeroMapped->setHealth(80);
        $superHeroMapped->setIntelligence(10);
        $superHeroMapped->setPower(50);

        $this->serializer
            ->shouldReceive('deserialize')
            ->with($json, SuperHero::class, 'json')
            ->once()
            ->andReturn($superHeroUpdate);

        $this->superHeroMapper
            ->shouldReceive('mapSuperHero')
            ->with($superHeroUpdate, $superHero)
            ->once()
            ->andReturn($superHeroMapped);

        $this->sut->getSuperHeroToUpdate($request, $superHero);

        $this->assertEquals($superHeroUpdate->getName(), $superHeroMapped->getName());
        $this->assertEquals($superHeroUpdate->getResidenceCity(), $superHeroMapped->getResidenceCity());
        $this->assertEquals($superHeroUpdate->getHealth(), $superHeroMapped->getHealth());
        $this->assertEquals($superHeroUpdate->getIntelligence(), $superHeroMapped->getIntelligence());
        $this->assertEquals($superHeroUpdate->getPower(), $superHeroMapped->getPower());
    }

    /** @test */
    public function insertSuperHero_whenSuperHero_andReturnTrue()
    {
        $superHero = new SuperHero();
        $superHero->setName("bart");
        $superHero->setResidenceCity('springfield');
        $superHero->setHealth(80);
        $superHero->setIntelligence(10);
        $superHero->setPower(50);
        $superHero->setKind('villano');
        $superHero->setCreatedAt(new \DateTime("now"));
        $superHero->setUpdatedAt(new \DateTime("now"));
        $superHero->setActive(true);
        $superHero->setCreatedBy(1);
        $superHero->setModifiedBy(1);

        $this->em
            ->shouldReceive('persist')
            ->with($superHero)
            ->once();

        $this->em
            ->shouldReceive('flush')
            ->once();

        $result = $this->sut->insertSuperHero($superHero);

        $this->assertTrue($result);
    }

    /** @test */
    public function insertSuperHero_whenSuperHeroNotSet_andReturnFalse()
    {
        $superHero = null;

        $result = $this->sut->insertSuperHero($superHero);

        $this->assertFalse($result);
    }


    /** @test */
    public function getListSuperHero_andReturnSuperHeroList()
    {
        $repository = m::mock(ObjectRepository::class);
        $SuperHeroArray = "";
        $this->em
            ->shouldReceive('getRepository')
            ->with(SuperHero::class)
            ->once()
            ->andReturn($repository);

        $repository
            ->shouldReceive('findAllSuperHeroActivated')
            ->once()
            ->andReturn($SuperHeroArray);

        $result = $this->sut->getListSuperHero();

        $this->assertEquals("", $result);
    }


    /** @test */
    public function getSuperHeroById_whenId_andReturnSuperHero()
    {
        $id = 1;
        $superHero = new SuperHero();
        $superHero->setName('spiderman');
        $superHero->setResidenceCity('Jaén');
        $superHero->setPower(75);
        $superHero->setIntelligence(90);
        $superHero->setHealth(99);
        $superHero->setKind('villano');
        $superHero->setActive(1);
        $superHero->setCreatedAt(new \DateTime('2018-12-03 00:00:00'));
        $superHero->setUpdatedAt(new \DateTime('2018-12-12 17:55:45'));
        $superHero->setCreatedBy(1);
        $superHero->setModifiedBy(1);

        $repository = m::mock(ObjectRepository::class);
        $this->em
            ->shouldReceive('getRepository')
            ->with(SuperHero::class)
            ->once()
            ->andReturn($repository);

        $repository
            ->shouldReceive('findOneBy')
            ->with(['id' => $id])
            ->once()
            ->andReturn($superHero);

        $result = $this->sut->getSuperHeroById($id);

        $this->assertEquals($superHero, $result);
    }

    /** @test */
    public function removeSuperHero_whenSuperHero_andReturnTrue()
    {
        $superHero = new SuperHero();
        $superHero->setActive(1);

        $result = $this->sut->removeSuperHero($superHero);

        $this->assertTrue($result);
    }


    protected function setUp()
    {
        $this->serializer = m::mock(SerializerInterface::class);
        $this->em = m::mock(EntityManagerInterface::class);
        $this->validator = m::mock(ValidatorInterface::class);
        $this->superHeroMapper = m::mock(SuperHeroMapperInterface::class);

        $this->sut = new SuperHeroManager(
            $this->serializer,
            $this->em,
            $this->validator,
            $this->superHeroMapper
        );
    }

    public function tearDown()
    {
        m::close();
    }
}