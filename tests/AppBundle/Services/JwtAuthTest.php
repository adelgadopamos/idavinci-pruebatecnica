<?php

namespace AppBundle\Tests\Services;

use AppBundle\Services\JwtAuth;
use AppBundle\Services\JwtAuthInterface;
use BackendBundle\Entity\User;
use Doctrine\Common\Persistence\ObjectRepository;
use Doctrine\ORM\EntityManagerInterface;
use Mockery as m;

class JwtAuthTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var JwtAuthInterface */
    private $sut;

    /** @var EntityManagerInterface | m\MockInterface */
    private $em;

    /** @test */
    public function signUp_whenUserNotValid_andReturnError()
    {
        $email = 'email@novalido.com';
        $password = 'noExisteEstaPassword';
        $user = null;

        $repository = m::mock(ObjectRepository::class);
        $this->em
            ->shouldReceive('getRepository')
            ->with(User::class)
            ->once()
            ->andReturn($repository);

        $repository->shouldReceive('findOneBy')
            ->with(["email" => $email, "password" => $password])
            ->once()
            ->andReturn($user);

        $data = [
            'status' => 'error',
            'code' => 400,
            'data' => 'Login failed!!'
        ];

        $result = $this->sut->signUp($email, $password, null);

        $this->assertEquals($data, $result);
    }

    protected function setUp()
    {
        $this->em = m::mock(EntityManagerInterface::class);

        $this->sut = new JwtAuth(
            $this->em
        );
    }

    public function tearDown()
    {
        m::close();
    }
}