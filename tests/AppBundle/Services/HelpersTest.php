<?php

namespace AppBundle\Tests\Services;

use AppBundle\Services\Helpers;
use AppBundle\Services\HelpersInterface;
use Mockery as m;
use Symfony\Component\Serializer\SerializerInterface;

class HelpersTest extends \Mockery\Adapter\Phpunit\MockeryTestCase
{
    /** @var HelpersInterface */
    private $helpers;

    /** @var SerializerInterface | m\MockInterface */
    private $serializer;

    /** @test */
    public function json_whenData_andReturnResponse()
    {
        $data = ['1' => 'one'];
        $json = '{"1": "one"}';
        $this->serializer
            ->shouldReceive('serialize')
            ->with($data, 'json')
            ->once()
            ->andReturn($json);

        $resultResponse = $this->helpers->json($data);

        $this->assertEquals($json, $resultResponse->getContent());
        $this->assertEquals('application/json', $resultResponse->headers->get('Content-Type'));
    }

    protected function setUp()
    {
        $this->serializer = m::mock(SerializerInterface::class);

        $this->helpers = new Helpers(
            $this->serializer
        );
    }

    public function tearDown()
    {
        m::close();
    }
}