<?php

namespace AppBundle\Manager;

use AppBundle\Mapper\UserMapperInterface;
use BackendBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class UserManager implements UserManagerInterface
{
    /** @var SerializerInterface */
    private $serializer;
    /** @var EntityManagerInterface */
    private $em;
    /** @var ValidatorInterface */
    private $validator;
    /** @var UserMapperInterface */
    private $userMapper;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        UserMapperInterface $userMapper
    )
    {

        $this->serializer = $serializer;
        $this->em = $em;
        $this->validator = $validator;
        $this->userMapper = $userMapper;
    }

    public function newUser(Request $request)
    {
        $json = $request->get("json", null);

        $user = $this->serializer->deserialize($json, User::class, 'json');
        $user->setCreatedAt(new \DateTime("now"));
        $user->setRole('user');

        $validationResult = $this->validator->validate($user);

        if (count($validationResult) == 0) {

            return $user;
        }

        return null;
    }

    public function getUserToUpdate(Request $request, $user)
    {
        $json = $request->get("json", null);

        $userUpdate = $this->serializer->deserialize($json, User::class, 'json');

        return $this->userMapper->mapUser($userUpdate, $user);

    }


    public function checkExistsUser($user)
    {
        $pwd = hash('sha256', $user->getPassword());
        $user->setPassword($pwd);

        $isSetUser = $this->em->getRepository(User::class)
            ->findBy([
                'email' => $user->getEmail()
            ]);

        if (count($isSetUser) == 0) {
            return true;
        }

        return null;
    }


    public function insertUser($user)
    {

        if (isset($user)) {
            $this->em->persist($user);
            $this->em->flush();

            return true;
        }

        return false;
    }


}