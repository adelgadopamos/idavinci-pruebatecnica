<?php

namespace AppBundle\Manager;

use BackendBundle\Entity\SuperHero;
use Symfony\Component\HttpFoundation\Request;

interface SuperHeroManagerInterface
{
    public function newSuperHero(Request $request): ?SuperHero;

    public function getSuperHeroToUpdate (Request $request, SuperHero $superHero);

    public function insertSuperHero(?SuperHero $superHero): bool;

    public function getListSuperHero();

    public function getSuperHeroById($id = null);

    public function removeSuperHero(SuperHero $superHero): bool;

}