<?php

namespace AppBundle\Manager;

use AppBundle\Mapper\SuperHeroMapper;
use AppBundle\Mapper\SuperHeroMapperInterface;
use BackendBundle\Entity\SuperHero;
use Doctrine\ORM\EntityManagerInterface;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Serializer\SerializerInterface;
use Symfony\Component\Validator\Validator\ValidatorInterface;

class SuperHeroManager implements SuperHeroManagerInterface
{
    /** @var SerializerInterface */
    private $serializer;
    /** @var EntityManagerInterface */
    private $em;
    /** @var ValidatorInterface */
    private $validator;
    /** @var SuperHeroMapper */
    private $superHeroMapper;

    public function __construct(
        SerializerInterface $serializer,
        EntityManagerInterface $em,
        ValidatorInterface $validator,
        SuperHeroMapperInterface $superHeroMapper
    )
    {
        $this->serializer = $serializer;
        $this->em = $em;
        $this->validator = $validator;
        $this->superHeroMapper = $superHeroMapper;
    }


    public function newSuperHero(Request $request): ?SuperHero
    {
        $json = $request->get("json", null);

        $superHero = $this->serializer->deserialize($json, SuperHero::class, 'json');
        $superHero->setCreatedAt(new \DateTime("now"));
        $superHero->setUpdatedAt(new \DateTime("now"));
        $superHero->setActive(true);
        $superHero->setCreatedBy(1);
        $superHero->setModifiedBy(1);

        $validationResult = $this->validator->validate($superHero);

        if (count($validationResult) == 0) {
            return $superHero;
        }

        return null;

    }

    public function getSuperHeroToUpdate(Request $request, $superHero)
    {
        $json = $request->get("json", null);

        $superHeroUpdate = $this->serializer->deserialize($json, SuperHero::class, 'json');

        return $this->superHeroMapper->mapSuperHero($superHeroUpdate, $superHero);
    }

    public function insertSuperHero(?SuperHero $superHero): bool
    {

        if (isset($superHero)) {
            $this->em->persist($superHero);
            $this->em->flush();

            return true;
        }

        return false;
    }

    public function getListSuperHero()
    {
        return $this->em
            ->getRepository(SuperHero::class)
            ->findAllSuperHeroActivated();
    }

    public function getSuperHeroById($id = null)
    {

        return $this->em
            ->getRepository(SuperHero::class)
            ->findOneBy([
                'id' => $id
            ]);
    }

    public function removeSuperHero(SuperHero $superHero): bool
    {
        $superHero->setActive(0);

        return true;
    }

}