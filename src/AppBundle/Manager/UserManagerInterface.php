<?php

namespace AppBundle\Manager;

use Symfony\Component\HttpFoundation\Request;

interface UserManagerInterface
{

    public function newUser(Request $request);

    public function getUserToUpdate (Request $request, $user);

    public function checkExistsUser ($user);

    public function insertUser ($user);
}