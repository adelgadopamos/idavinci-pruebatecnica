<?php

namespace AppBundle\Manager;

use AppBundle\Services\HelpersInterface;

class ResponseManager implements ResponseManagerInterface
{
    const STATUS = 'status';
    const CODE = 'code';
    const MSG = 'msg';
    const OBJECT = 'object';
    const ERROR_STATUS = 'error';
    const SUCCESS_STATUS = 'success';
    const TOTAL_ITEMS_COUNT = 'total_items_count';
    const PAGE_ACTUAL = 'page_actual';
    const ITEMS_PER_PAGE = 'items_per_page';
    const TOTAL_PAGES = 'total_pages';
    const DATA = 'data';
    const ERROR_CODE = 400;
    const SUCCESS_CODE = 200;

    /** @var HelpersInterface */
    private $helpers;

    public function __construct(HelpersInterface $helpers)
    {
        $this->helpers = $helpers;
    }

    public function getArrayErrorResponse(string $msg)
    {
        return $this->helpers->json([
            self::STATUS => self::ERROR_STATUS,
            self::CODE => self::ERROR_CODE,
            self::MSG => $msg
        ]);
    }

    public function getArraySuccessResponse(string $msg)
    {
        return $this->helpers->json([
            self::STATUS => self::SUCCESS_STATUS,
            self::CODE => self::SUCCESS_CODE,
            self::MSG => $msg
        ]);

    }

    public function getArraySuccessResponseObject($object)
    {
        return $this->helpers->json([
            self::STATUS => self::SUCCESS_STATUS,
            self::CODE => self::SUCCESS_CODE,
            self::OBJECT => $object
        ]);
    }

    public function getArraySuccessResponsePagination($total_items_count, $page, $items_per_page, $pagination)
    {
        return $this->helpers->json([
            self::STATUS => self::SUCCESS_STATUS,
            self::CODE => self::SUCCESS_CODE,
            self::TOTAL_ITEMS_COUNT => $total_items_count,
            self::PAGE_ACTUAL => $page,
            self::ITEMS_PER_PAGE => $items_per_page,
            self::TOTAL_PAGES => ceil($total_items_count / $items_per_page),
            self::DATA => $pagination
        ]);
    }
}