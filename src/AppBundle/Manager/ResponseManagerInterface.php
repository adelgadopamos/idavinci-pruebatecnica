<?php

namespace AppBundle\Manager;

interface ResponseManagerInterface
{

    public function getArrayErrorResponse(string $msg);

    public function getArraySuccessResponse(string $msg);

    public function getArraySuccessResponseObject($object);

    public function getArraySuccessResponsePagination($total_items_count, $page, $items_per_page, $pagination);

}