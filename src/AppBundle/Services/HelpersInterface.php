<?php

namespace AppBundle\Services;

use Symfony\Component\HttpFoundation\Response;

interface HelpersInterface
{
    public function json(array $data): Response;
}