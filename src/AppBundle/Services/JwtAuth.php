<?php

namespace AppBundle\Services;

use BackendBundle\Entity\User;
use Doctrine\ORM\EntityManagerInterface;
use Firebase\JWT\JWT;

class JwtAuth implements JwtAuthInterface
{
    /** @var string */
    const ENCODE = 'HS256';
    /** @var EntityManagerInterface */
    public $manager;
    /** @var string */
    public $key;

    public function __construct(EntityManagerInterface $manager)
    {
        $this->manager = $manager;
        $this->key = '8383thjgjekgf94guhggkj44894';
    }

    public function signUp($email, $password, $getHash = null)
    {
        $user = $this->manager->getRepository(User::class)->findOneBy(
            [
                "email" => $email,
                "password" => $password
            ]);

        $data = [
            'status' => 'error',
            'code' => 400,
            'data' => 'Login failed!!'
        ];

        if (is_object($user)) {
            $token = [
                'sub' => $user->getId(),
                'email' => $user->getEmail(),
                'name' => $user->getName(),
                'surname' => $user->getSurname(),
                'iat' => time(),
                'exp' => time() + (7 * 24 * 60 * 60)
            ];

            $jwt = JWT::encode($token, $this->key, self::ENCODE);
            $data = JWT::decode($jwt, $this->key, [self::ENCODE]);

            if ($getHash == null) {
                $data = $jwt;
            }
        }

        return $data;
    }

    public function checkToken($jwt, $getIdentity = false)
    {
        $auth = false;
        try {
            $decoded = JWT::decode($jwt, $this->key, [self::ENCODE]);
        } catch (\UnexpectedValueException $e) {
            $auth = false;
        } catch (\DomainException $e) {
            $auth = false;
        }

        if (isset($decoded) && is_object($decoded) && isset($decoded->sub)) {
            $auth = true;
        }

        if ($getIdentity != false) {
            return $decoded;
        }

        return $auth;

    }


}
