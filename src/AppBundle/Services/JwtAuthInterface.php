<?php

namespace AppBundle\Services;

interface JwtAuthInterface
{

    public function signUp(string $email, string $password, $getHash = null);

    public function checkToken($jwt, $getIdentity = false);
}