<?php

namespace AppBundle\EventSubscriber;

use AppBundle\Services\JwtAuth;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\Event\GetResponseEvent;
use Symfony\Component\HttpKernel\Exception\AccessDeniedHttpException;
use Symfony\Component\HttpKernel\KernelEvents;

class AuthorizationSubscriber implements EventSubscriberInterface
{
    /**
     * @var JWTAuth
     */
    private $jwtAuth;

    public function __construct(JwtAuth $jwtAuth)
    {
        $this->jwtAuth = $jwtAuth;
    }


    public static function getSubscribedEvents()
    {
        return [
            KernelEvents::REQUEST => 'authorized'
        ];
    }

    public function authorized(GetResponseEvent $event)
    {
        $request = $event->getRequest();
        $pathInfo = $request->getPathInfo();

        if ($pathInfo != '/user/new' &&
            $pathInfo != '/login' &&
            $pathInfo != '/') {

            $token = $request->get('authorization', null);

            if (!$this->jwtAuth->checkToken($token)) {
                throw new AccessDeniedHttpException('Access Denied');
            }

        }
    }
}
