<?php

namespace AppBundle\Mapper;

use BackendBundle\Entity\SuperHero;

interface SuperHeroMapperInterface
{
    public function mapSuperHero(SuperHero $originSuperHero, SuperHero $superHero): ?SuperHero;
}