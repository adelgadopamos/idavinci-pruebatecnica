<?php

namespace AppBundle\Mapper;

use BackendBundle\Entity\User;

interface UserMapperInterface
{
    public function mapUser(User $originUser, User $user): ?User;
}