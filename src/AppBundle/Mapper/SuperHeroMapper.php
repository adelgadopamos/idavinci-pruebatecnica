<?php

namespace AppBundle\Mapper;

use BackendBundle\Entity\SuperHero;

class SuperHeroMapper implements SuperHeroMapperInterface
{
    public function mapSuperHero(SuperHero $originSuperHero, SuperHero $superHero): ?SuperHero
    {
        $superHero->setName($originSuperHero->getName());
        $superHero->setResidenceCity($originSuperHero->getResidenceCity());
        $superHero->setIntelligence($originSuperHero->getIntelligence());
        $superHero->setHealth($originSuperHero->getHealth());
        $superHero->setPower($originSuperHero->getPower());
        $superHero->setUpdatedAt(new \DateTime("now"));

        return $superHero;
    }
}