<?php

namespace AppBundle\Mapper;

use BackendBundle\Entity\User;

class UserMapper implements UserMapperInterface
{
    public function mapUser(User $originUser, User $user): ?User
    {
        $user->setName($originUser->getName());
        $user->setSurname($originUser->getSurname());
        $user->setEmail($originUser->getEmail());
        $user->setRole('user');

        if ($originUser->getPassword() != " ") {
            $pwd = hash('sha256', $originUser->getPassword());
            $user->setPassword($pwd);
        }

        return $user;
    }

}