<?php

namespace AppBundle\Controller;

use AppBundle\Manager\ResponseManager;
use AppBundle\Manager\UserManager;
use BackendBundle\Entity\User;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class UserController extends Controller
{
    const STATUS = 'status';
    const ERROR_STATUS = 'error';
    const SUCCESS_STATUS = 'success';
    const ERROR_CODE = 400;
    const SUCCESS_CODE = 200;

    /**
     * @Route ("/new", name="new_user")
     */
    public function newAction(Request $request)
    {
        $responseManager = $this->get(ResponseManager::class);

        $userManager = $this->get(UserManager::class);

        $userNew = $userManager->newUser($request);

        if ($userNew != null) {

            if ($userManager->checkExistsUser($userNew) == true) {
                $userManager->insertUser($userNew);

                return $responseManager->getArraySuccessResponseObject($userNew);
            }
        }

        return $responseManager->getArrayErrorResponse("User not created");
    }

    /**
     * @Route ("/edit/{id}", name="edit_user")
     */
    public function editAction(Request $request, User $user)
    {
        $responseManager = $this->get(ResponseManager::class);
        $userManager = $this->get(UserManager::class);

        $userUpdate = $userManager->getUserToUpdate($request, $user);

        $userManager->insertUser($userUpdate);

        return $responseManager->getArraySuccessResponseObject($userUpdate);

    }

}
