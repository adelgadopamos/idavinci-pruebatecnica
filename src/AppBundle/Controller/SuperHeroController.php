<?php

namespace AppBundle\Controller;


use AppBundle\Manager\ResponseManager;
use AppBundle\Manager\SuperHeroManager;
use BackendBundle\Entity\SuperHero;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class SuperHeroController extends Controller
{
    const SUPER_HERO_NOT_FOUND = 'SuperHero not found';
    const SUPER_HERO_REMOVED_SUCCESS = "SuperHero removed success";


    /**
     * @Route("/new", name="new_superhero")
     */
    public function newAction(Request $request)
    {
        $responseManager = $this->get(ResponseManager::class);
        $superHeroManager = $this->get(SuperHeroManager::class);

        $newSuperHero = $superHeroManager->newSuperHero($request);

        if ($newSuperHero != null) {
            $superHeroManager->insertSuperHero($newSuperHero);

            return $responseManager->getArraySuccessResponseObject($newSuperHero);
        }

        return $responseManager->getArrayErrorResponse("SuperHero exists");

    }

    /**
     * @Route("/edit/{id}", name="edit_superhero")
     */
    public function editAction(Request $request, SuperHero $superHero)
    {
        $responseManager = $this->get(ResponseManager::class);
        $superHeroManager = $this->get(SuperHeroManager::class);

        $superHeroUpdate = $superHeroManager->getSuperHeroToUpdate($request, $superHero);

        $superHeroManager->insertSuperHero($superHeroUpdate);

        return $responseManager->getArraySuccessResponseObject($superHeroUpdate);

    }


    /**
     * @Route("/list", name="list_superhero")
     */
    public function listAction(Request $request)
    {
        $responseManager = $this->get(ResponseManager::class);

        $superHeroManager = $this->get(SuperHeroManager::class);
        $query = $superHeroManager->getListSuperHero();

        $page = $request->query->getInt('page', 1);
        $paginator = $this->get('knp_paginator');
        $items_per_page = 10;

        $pagination = $paginator->paginate($query, $page, $items_per_page);
        $total_items_count = $pagination->getTotalItemCount();

        return $responseManager
            ->getArraySuccessResponsePagination($total_items_count, $page, $items_per_page, $pagination);

    }


    /**
     * @Route("/detail/{id}", name="detail_superhero")
     */
    public function superHeroAction(SuperHero $superHero)
    {
        $responseManager = $this->get(ResponseManager::class);

        if ($superHero->getActive() == 1) {
            return $responseManager->getArraySuccessResponseObject($superHero);
        }

        return $responseManager->getArrayErrorResponse(self::SUPER_HERO_NOT_FOUND);
    }


    /**
     * @Route("/remove/{id}", name="remove_superhero")
     */
    public function removeAction($id = null)
    {
        $responseManager = $this->get(ResponseManager::class);

        $superHeroManager = $this->get(SuperHeroManager::class);
        $superHero = $superHeroManager->getSuperHeroById($id);

        if ($superHero && is_object($superHero) && $superHero->getActive() == 1) {

            $isRemoved = $superHeroManager->removeSuperHero($superHero);

            if ($isRemoved == true) {
                $superHeroManager->insertSuperHero($superHero);

                return $responseManager->getArraySuccessResponse(self::SUPER_HERO_REMOVED_SUCCESS);
            }
        }

        return $responseManager->getArrayErrorResponse(self::SUPER_HERO_NOT_FOUND);
    }

}
