<?php

namespace AppBundle\Controller;

use AppBundle\Services\JwtAuth;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\Routing\Annotation\Route;

class DefaultController extends Controller
{
    const SUCCESS_STATUS = 'success';
    const ERROR_STATUS = 'error';
    const STATUS = 'status';

    /**
     * @Route("/", name="index")
     */
    public function indexAction()
    {
        return $this->render('default/index.html.twig', [
            'base_dir' => realpath($this->getParameter('kernel.project_dir')) . DIRECTORY_SEPARATOR,
        ]);
    }

    /**
     * @Route("/login", name="login_default")
     */
    public function loginAction(Request $request)
    {
        $json = $request->get('json', null);

        $params = json_decode($json);

        $email = (isset($params->email)) ? $params->email : null;
        $password = (isset($params->password)) ? $params->password : null;
        $getHash = (isset($params->getHash)) ? $params->getHash : null;

        $pwd = hash('sha256', $password);

        $jwt_auth = $this->get(JwtAuth::class);

        if ($getHash == null || $getHash == false) {
            $signUp = $jwt_auth->signUp($email, $pwd);
        } else {
            $signUp = $jwt_auth->signUp($email, $pwd, true);
        }

        return $this->json($signUp);

    }


}

