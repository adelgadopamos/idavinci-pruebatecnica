<?php

namespace BackendBundle\Repository;

use BackendBundle\Entity\SuperHero;
use Doctrine\ORM\EntityRepository;

class SuperHeroRepository extends EntityRepository
{
    public function findAllSuperHeroActivated(){

        return $this->getEntityManager()
            ->createQueryBuilder()
            ->select('s')
            ->from(SuperHero::class, 's')
            ->where('s.active=1')
            ->getQuery();
    }
}