<?php

namespace BackendBundle\Entity;
use Symfony\Bridge\Doctrine\Validator\Constraints\UniqueEntity;
use Symfony\Component\Validator\Constraints as Assert;

/**
 * Superpersonas
 * @UniqueEntity("name")
 */
class SuperHero
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     * @Assert\NotBlank()
     */
    private $name;

    /**
     * @var string
     */
    private $residenceCity;

    /**
     * @var integer
     */
    private $power = '0';

    /**
     * @var integer
     */
    private $intelligence = '0';

    /**
     * @var integer
     */
    private $health = '0';

    /**
     * @var string
     */
    private $kind;

    /**
     * @var boolean
     */
    private $active;

    /**
     * @var \DateTime
     */
    private $createdAt;

    /**
     * @var \DateTime
     */
    private $updatedAt;

    /**
     * @var integer
     * @Assert\NotBlank()
     */
    private $createdBy;

    /**
     * @var integer
     */
    private $modifiedBy;


    /**
     * Get id
     *
     * @return integer
     */
    public function getId(): ?int
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Superpersonas
     */
    public function setName(string $name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName(): string
    {
        return $this->name;
    }

    /**
     * Set residenceCity
     *
     * @param string $residenceCity
     *
     * @return Superpersonas
     */
    public function setResidenceCity(string $residenceCity)
    {
        $this->residenceCity = $residenceCity;

        return $this;
    }

    /**
     * Get residenceCity
     *
     * @return string
     */
    public function getResidenceCity(): string
    {
        return $this->residenceCity;
    }

    /**
     * Set power
     *
     * @param integer $power
     *
     * @return Superpersonas
     */
    public function setPower(int $power)
    {
        $this->power = $power;

        return $this;
    }

    /**
     * Get power
     *
     * @return integer
     */
    public function getPower(): int
    {
        return $this->power;
    }

    /**
     * Set intelligence
     *
     * @param integer $intelligence
     *
     * @return Superpersonas
     */
    public function setIntelligence(int $intelligence)
    {
        $this->intelligence = $intelligence;

        return $this;
    }

    /**
     * Get intelligence
     *
     * @return integer
     */
    public function getIntelligence(): int
    {
        return $this->intelligence;
    }

    /**
     * Set health
     *
     * @param integer $health
     *
     * @return Superpersonas
     */
    public function setHealth(int $health)
    {
        $this->health = $health;

        return $this;
    }

    /**
     * Get health
     *
     * @return integer
     */
    public function getHealth(): int
    {
        return $this->health;
    }

    /**
     * Set kind
     *
     * @param string $kind
     *
     * @return Superpersonas
     */
    public function setKind(string $kind)
    {
        $this->kind = $kind;

        return $this;
    }

    /**
     * Get kind
     *
     * @return string
     */
    public function getKind(): string
    {
        return $this->kind;
    }

    /**
     * Set active
     *
     * @param boolean $active
     *
     * @return Superpersonas
     */
    public function setActive(bool $active)
    {
        $this->active = $active;

        return $this;
    }

    /**
     * Get active
     *
     * @return boolean
     */
    public function getActive(): bool
    {
        return $this->active;
    }

    /**
     * Set createdAt
     *
     * @param \DateTime $createdAt
     *
     * @return Superpersonas
     */
    public function setCreatedAt(\DateTime $createdAt)
    {
        $this->createdAt = $createdAt;

        return $this;
    }

    /**
     * Get createdAt
     *
     * @return \DateTime
     */
    public function getCreatedAt()
    {
        return $this->createdAt;
    }

    /**
     * Set updatedAt
     *
     * @param \DateTime $updatedAt
     *
     * @return Superpersonas
     */
    public function setUpdatedAt(\DateTime $updatedAt)
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }

    /**
     * Get updatedAt
     *
     * @return \DateTime
     */
    public function getUpdatedAt()
    {
        return $this->updatedAt;
    }

    /**
     * Set createdBy
     *
     * @param integer $createdBy
     *
     * @return Superpersonas
     */
    public function setCreatedBy(int $createdBy)
    {
        $this->createdBy = $createdBy;

        return $this;
    }

    /**
     * Get createdBy
     *
     * @return integer
     */
    public function getCreatedBy(): int
    {
        return $this->createdBy;
    }

    /**
     * Set modifiedBy
     *
     * @param integer $modifiedBy
     *
     * @return Superpersonas
     */
    public function setModifiedBy(int $modifiedBy)
    {
        $this->modifiedBy = $modifiedBy;

        return $this;
    }

    /**
     * Get modifiedBy
     *
     * @return integer
     */
    public function getModifiedBy(): ?int
    {
        return $this->modifiedBy;
    }
}

